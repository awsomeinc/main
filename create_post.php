<?php
	session_start();
	if (!isset($_SESSION['user-email'])){ // Checking if user unauthorized
		header("Location: index.php");
	}
?>

	<!DOCTYPE html>
	<html lang="en">

	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="Art news, gallery">
		<meta name="keywords" content="art, artwork, photography, calligraphy, sculpture, printmaking, decorate">
		<meta name="author" content="Nikita Shkarupa">
		<link rel="icon" href="img/favicon.ico">
		<title>Awesome • Beautiful photos</title>

		<!-- Libs, fonts and custom css -->
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">

		<!-- Datepicker -->
		<link href="libs/datepicker/dist/css/datepicker.css" rel="stylesheet" type="text/css">

		<!-- Custom css -->
		<link href="css/main.css" rel="stylesheet" type="text/css" />
		<link href="css/create_post.css" rel="stylesheet" type="text/css" />
		<link href="css/media.css" rel="stylesheet" type="text/css" />

		<!-- Markdown to HTML lib-->
		<script src="node_modules/markdown/lib/markdown.js"></script>
	</head>

	<body>

		<!-- Trying to load data fron local storage -->
		<script>
			window.onload = function() {
				var inputs = ["#heading", "#sel1", "#photo_url", "#preview_text", "#text_input"];
				$.each(inputs, function(i, l) {
					var value = localStorage.getItem(l);
					if (value !== null) $(l).val(value);
				});
			}

		</script>

		<!-- HEADER -->
		<?php include 'components/header.php';?>

		<!-- Container -->
		<div class="container marketing">

			<!-- Page Content -->
			<div class="container">
				<h2>Create new post</h2>
				<hr>
				<form class="form" name="create-post-form" action="/awesome/logic/adding-post.php" method="POST" onsubmit="return CreatePostValidation()">

					<!-- Post heading input -->
					<div class="row create-input">
						<div class="col-md-4">
							<label for="heading">
								<h5>Heading</h5>
							</label>
						</div>
						<div class="col-md-8">
							<input name="heading" id="heading" type="text" class="form-control" autofocus required>
							<span class="text-muted float-right">Max 20 characters.</span>
						</div>
					</div>

					<!-- Post categorie selection -->
					<div class="row create-input">
						<div class="col-md-4">
							<label for="sel1">
								<h5>Select categorie</h5>
							</label>
						</div>
						<div class="col-md-8">
							<select name="categorie" class="form-control" id="sel1">
							<option selected>Animals</option>
							<option>Architecture</option>
							<option>City</option>
							<option>Nature</option>
							<option>People</option>
							<option>Sea</option>
						</select>
						</div>
					</div>

					<!-- Preview photo URL -->
					<div class="row create-input">
						<div class="col-md-4">
							<label for="photo-url">
								<h5>Choose preview photo</h5>
							</label>
						</div>
						<div class="col-md-8">
							<input name="preview-photo" id="photo_url" type="text" class="form-control" required>
							<span class="text-muted float-right">Paste image URl. The best size for preview image is 700x400.</span>
						</div>
					</div>

					<!-- Preview text input -->
					<div class="row create-input">
						<div class="col-md-4">
							<label for="preview-text">
								<h5>Preview text</h5>
							</label>
						</div>
						<div class="col-md-8">
							<textarea name="preview-text" class="form-control" id="preview_text" rows="5" required></textarea>
							<span class="text-muted float-right">Write a couple sentences about your post.</span>
						</div>
					</div>

					<!-- Post markdown -->
					<div class="row create-input">
						<div class="col-md-12">
							<div class="md-heading">
								<label for="text-input">
										<h5>Now write your post using markdown</h5>
									</label>
								<a href="https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet" target="_blank">"Markdown Cheatsheet" on github</a>
							</div>
							<textarea name="markdown" id="text_input" class="form-control" oninput="this.editor.update()" rows="15" required>Type **Markdown** here.</textarea>
						</div>
					</div>

					<!--MD2HTML preview-->
					<div class="row">
						<div class="col-md-12">
							<h5>Result:</h5>
							<div class="form-control" id="preview" style="overflow:scroll; height:600px;"></div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<button class="btn btn-lg btn-primary float-right" type="submit" id="save-post">Save</button>
						</div>
					</div>
				</form>

				<!-- Markdown to HTML script, makes preview markdown working -->
				<script>
					function Editor(input, preview) {
						this.update = function() {
							preview.innerHTML = markdown.toHTML(input.value);
						};
						input.editor = this;
						this.update();
					}
					var $ = function(id) {
						return document.getElementById(id);
					};
					new Editor($("text_input"), $("preview"));

				</script>

				<!-- Saving inputs' value to the local storage befor leaving the page -->
				<script>
					window.onbeforeunload = function() {
						var inputs = ["#heading", "#sel1", "#photo_url", "#preview_text", "#text_input"];
						$.each(inputs, function(i, l) {
							var value = $(l).val();
							localStorage.setItem(l, value);
						});
					}

				</script>

			</div>
			<!-- /Container -->

			<!-- FOOTER -->
			<hr class="featurette-divider">
			<footer>
				<p class="float-right"><a id="to-top" href="#">Back to top</a></p>
				<p>&copy; 2017 Awesome, Inc.</p>
			</footer>

		</div>


		<!-- Bootstrap core JavaScript -->
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>

		<!-- Other Scripts -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="js/bootstrap.js"></script>
		<script src="https://use.fontawesome.com/1621f9eb2b.js"></script>

		<!-- Custom JavaScript -->
		<script src="js/common.js"></script>

	</body>

	</html>
