-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 15, 2017 at 11:50 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Awesome`
--

-- --------------------------------------------------------

--
-- Table structure for table `COMMENT`
--

CREATE TABLE `COMMENT` (
  `id` int(11) NOT NULL,
  `post` char(20) NOT NULL,
  `author` char(20) NOT NULL,
  `date` datetime NOT NULL,
  `text` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `COMMENT`
--

INSERT INTO `COMMENT` (`id`, `post`, `author`, `date`, `text`) VALUES
(1, 'Australian Animals 2', 'skarunik@fel.cvut.cz', '2017-10-15 20:10:29', 'Test comment'),
(2, 'Australian Animals 2', 'skarunik@fel.cvut.cz', '2017-10-15 20:10:36', 'Test comment 2');

-- --------------------------------------------------------

--
-- Table structure for table `PERSON`
--

CREATE TABLE `PERSON` (
  `email` char(20) NOT NULL,
  `firstname` text NOT NULL,
  `lastname` text NOT NULL,
  `password` text NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `birthday` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `PERSON`
--

INSERT INTO `PERSON` (`email`, `firstname`, `lastname`, `password`, `admin`, `birthday`) VALUES
('adam@gmail.com', 'Adam', 'Handriks', '$2y$10$CB1/.oJgUUnj4zn2.3ImBOg2UYW37fXbES1Sqcg.0Hw7WHKq9Yguy', 0, '1984-09-05'),
('jole@gmail.com', 'Jolene', 'Green', '$2y$10$A9hv.qQCQPlzWPjY0uWqguXrXPXGTuunuKha1AiIn8YeVegOEh8mq', 0, '1993-08-17'),
('skarunik@fel.cvut.cz', 'Nikita', 'Shkarupa', '$2y$10$uA46giKvcoPhMQpGOxBfUuv.gnZ7SWAftMPBc4IXacmlCErQmsJey', 1, '1997-04-04'),
('tester@email.com', 'Test', 'User', '$2y$10$KUk5b.MjY1O1cCKhIT6p1.L23iFFfWb6KZGYajfqL4BAwcH1y4i2a', 0, '2017-10-19');

-- --------------------------------------------------------

--
-- Table structure for table `POST`
--

CREATE TABLE `POST` (
  `heading` char(20) NOT NULL,
  `author` char(20) NOT NULL,
  `date` datetime NOT NULL,
  `categorie` char(20) NOT NULL,
  `previewPhoto` text NOT NULL,
  `previewText` text NOT NULL,
  `markdown` mediumtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `POST`
--

INSERT INTO `POST` (`heading`, `author`, `date`, `categorie`, `previewPhoto`, `previewText`, `markdown`) VALUES
('Australian Animals', 'skarunik@fel.cvut.cz', '2017-10-15 01:11:00', 'Animals', 'http://i.dailymail.co.uk/i/pix/2016/05/04/12/33C9A74D00000578-3571189-image-a-27_1462360771305.jpg', 'In Australia we have many native animals. Some are very famous, such as koalas and kangaroos, while others may not be familiar to visitors. ', '![pic](https://cdn.theculturetrip.com/wp-content/uploads/2016/12/7828505280_ac055aa351_k.jpg)\r\n##Mammalsâ€¨\r\n\r\nAustraliaâ€™s mammalian wildlife is unique from the rest of the world. The dingo, or wild dog, is our largest carnivorous mammal, while the numbat, quoll and Tasmanian devil are each generally the size of an average house cat. \r\n\r\nEndangered quolls are difficult to spot in the wild, but inhabit the wet forests of southeastern Australia and Tasmania, and a small area of northern Queensland. The bilby can be seen in Francois Peron National Park in Western Australia.\r\n\r\n##Marsupials\r\n\r\nAustralia has more than 140 species of marsupials, including kangaroos, wallabies, koalas, and wombats. Our 55 different native species of kangaroos and wallabies vary greatly in size and weight, ranging from half a kilogram (1 pound) to 90 (198.4 pounds) kilograms. \r\n\r\n##Monotremes\r\n\r\nAnother animal group found only in Australia is the monotremes, or egg-laying mammals. The most distinctive is the platypus, a river-dwelling animal with a bill like a duck, a furry waterproof body and webbed feet. Platypuses live in burrows, which they dig into the banks of rivers. They are difficult to spot, but your best chance to see them is in small streams and calm rivers along the east coast, such as the Tidbinbilla Nature Reserve near Canberra, Lake Elizabeth in Victoriaâ€™s Great Otway National Park and in northern New South Wales and Queensland.\r\n\r\n##Birds\r\n\r\nWe have more than 800 species of birds in Australia, and about half cannot be found anywhere else. They range from tiny honeyeaters to the large, flightless emu, which stands nearly two metres (6.6 feet) tall. See cassowaries in our tropical rainforest, kookaburras in our open woodlands and emus in sclerophyll forests and savanna woodlands. Get up close to penguins on Kangaroo Island in South Australia and Philip Island in \r\n, or hear the winter call of the lyrebirds in Wollumbin National Park and in the Gondwana Rainforests in southeast Queensland.'),
('Australian Animals 2', 'skarunik@fel.cvut.cz', '2017-10-15 01:17:37', 'Animals', 'https://i.kinja-img.com/gawker-media/image/upload/t_original/tzi34fjcirichmuh24lf.jpg', 'In Australia we have many native animals. Some are very famous, such as koalas and kangaroos, while others may not be familiar to visitors. ', '![croc](http://reptilepark.com.au/wp-content/uploads/2015/12/crocodiles_saltwater2.jpg)\r\n##Reptiles\r\n\r\nAustralia has more venomous snakes than any other continent, 21 of the worldâ€™s 25 deadliest in fact. But not all are poisonous, and we also have some stunning pythons and tree snakes. We are famous for our crocodiles, and host two different species, the freshwater crocodile, which is found nowhere else in the world, and the estuarine crocodile (also known as the saltwater crocodile). The Kimberley, Kakadu National Park and Cape York Peninsula are excellent places to see crocodiles in their natural habitat. \r\n\r\nOf the seven species of marine turtles in the world, six can be found here including the flatback turtle, green turtle, hawksbill turtle, leatherback turtle, loggerhead turtle and olive ridley turtle. The best spots to see turtles are Ningaloo Reef in Western Australia and Eco Beach in Broom.\r\n\r\nWe also have an amazing array of lizards, â€˜dragonsâ€™ and goannas (monitor lizards), such as the spectacular frilled-neck lizard and bearded dragon. Thorny devils can be found in Western Australiaâ€™s desert habitats, Shark Bay, Carnarvon and Exmouth. \r\n\r\n##Marine animals\r\n\r\nOur marine environments support around 4,000 of the worldâ€™s 22,000 types of fish, as well as 30 of the worldâ€™s 58 seagrass species. We also have the worldâ€™s largest coral reef system, the World Heritage-listed \r\n\r\n, where there are countless species of colourful fish, including the beautiful clownfish seen in Finding Dory. We also have around 1,700 different species of coral.\r\n\r\nLarger marine species include the humpback, southern right and orca whales, the dugong (or manatee), several dolphin species and a number of different sharks. Catch a glimpse of the whales during their migration along the east coast from May to November, or swim with gentle whale sharks on Ningaloo Reef in Western Australia.\r\n\r\nKangaroo Island is one of the best places to see beautiful Australian fur seals in the wild.'),
('Chilly Willy', 'skarunik@fel.cvut.cz', '2017-09-15 18:24:39', 'Animals', 'http://localhost/awesome/img/post-preview/bird.jpg', 'Phasellus ac sapien purus. Cras quis dui lacinia, sagittis neque at, rutrum ante. Vestibulum pulvinar tincidunt mauris in interdum. Proin diam urna, tincidunt vitae commodo nec, tristique vel dui.', 'Chilly Willy\r\n========\r\n\r\nPhasellus ac sapien purus. Cras quis dui lacinia, sagittis neque at, rutrum ante. Vestibulum pulvinar tincidunt mauris in interdum. Proin diam urna, tincidunt vitae commodo nec, tristique vel dui.\r\n\r\nPhasellus ac sapien purus. Cras quis dui lacinia, sagittis neque at, rutrum ante. Vestibulum pulvinar tincidunt mauris in interdum. Proin diam urna, tincidunt vitae commodo nec, tristique vel dui.\r\n\r\n![](https://i2.wp.com/fulltiltnyr.com/wp-content/uploads/2014/05/chillywilly.jpg)\r\n\r\nPhasellus ac sapien purus. Cras quis dui lacinia, sagittis neque at, rutrum ante. Vestibulum pulvinar tincidunt mauris in interdum. Proin diam urna, tincidunt vitae commodo nec, tristique vel dui.\r\n\r\nPhasellus ac sapien purus. Cras quis dui lacinia, sagittis neque at, rutrum ante. Vestibulum pulvinar tincidunt mauris in interdum. Proin diam urna, tincidunt vitae commodo nec, tristique vel dui.\r\n\r\n![](http://www.traditionalanimation.com/wp-content/gallery/chilly-willy/chillywillymodelsheet2.jpg)'),
('Explore Architecture', 'skarunik@fel.cvut.cz', '2017-10-15 00:57:49', 'Architecture', 'https://www.architecture.com/-/media/113ec032a9bc4c62b8f539015a6f5ef6.png?h=307&amp;w=767&amp;la=en&amp;hash=1FF0CF33C7C9A7C21D1116D76A7ECC6256775239', 'A German composite set of brass drawing instruments', '![picture](https://www.architecture.com/-/media/gathercontent/set-of-brass-drawing-instruments-c1600/image-one/2015february24thbrassdrawinginstrumentsriba36333700x455pxjpg.jpg?h=455&amp;w=700&amp;la=en&amp;hash=16A0B580EB58C8FDAEFA9D9251B433E31C1879CF)\r\n\r\nThis is a German composite set of brass drawing instruments dating from c.1600 presented in a green leather case.\r\n\r\nThe set comprises many instruments including a protractor, set angle, large pencil compasses, parallel rule, sector, steel points, large and small dividers with inserts for either ink, graphite or steel. These delicately crafted instruments were utilised by architects as the essential tools of draughtsmanship and to calculate accurate geometry. The use of instruments like these and in particular the use of compasses to create precise mathematical proportion is illustrated beautifully in the drawing, Design for a Rose Window by Robert Smythson where the paper clearly shows the marks of the compasses and divider reference points.'),
('HI everyone', 'skarunik@fel.cvut.cz', '2017-10-15 01:02:34', 'Animals', 'http://www.clubpimble.com/uploads/headings_8619_69773.jpeg', 'Here at National Geographic Kids, we love wonderful wild bears! Join us as we learn about one of natureâ€™s cutest critters in our facts about pandas!', '![panda](https://searchengineland.com/figz/wp-content/seloads/2014/08/four-pandas-ss-1920-800x450.jpg)\r\n\r\n###Facts about pandas\r\n\r\n1. Giant pandas (often referred to as simply â€œpandasâ€) are black and white bears. In the wild, they are found in thick bamboo forests, high up in the mountains of central China â€“ you can check out our cool facts about China, here!\r\n\r\n2. These magnificent mammals are omnivores. But whilst they will occasionally eat small animals and fish, bamboo counts for 99 percent of their diet.\r\n\r\n3. These guys are BIG eaters â€“ every day they fill their tummies for up to 12 hours, shifting up to 12 kilograms of bamboo!\r\n\r\n4. The giant pandaâ€™s scientific name is Ailuropoda melanoleuca, which means â€œblack and white cat-footâ€.\r\n\r\n5. Giant pandas grow to between 1.2m and 1.5m, and weigh between 75kg and 135kg. Scientists arenâ€™t sure how long pandas live in the wild, but in captivity they live to be around 30 years old.\r\n\r\n6. Baby pandas are born pink and measure about 15cm â€“ thatâ€™s about the size of a pencil! They are also born blind and only open their eyes six to eight weeks after birth.\r\n\r\n7. Itâ€™s thought that these magnificent mammals are solitary animals, with males and females only coming together briefly to mate. Recent research, however, suggests that giant pandas occasionally meet outside of breeding season, and communicate with each other through scent marks and calls.'),
('October skateboardin', 'skarunik@fel.cvut.cz', '2017-09-15 18:17:26', 'People', 'http://localhost/awesome/img/post-preview/skate.jpg', 'Nam convallis dolor ut hendrerit rutrum. Praesent eu ipsum aliquet, fringilla dolor sit amet, suscipit enim. Aenean pharetra sagittis magna ut molestie. Ut aliquam massa eget magna feugiat congue.', 'October skateboarding\r\n============\r\n\r\nNam convallis dolor ut hendrerit rutrum. Praesent eu ipsum aliquet, fringilla dolor sit amet, suscipit enim. Aenean pharetra sagittis magna ut molestie. Ut aliquam massa eget magna feugiat congue.\r\n\r\nNam convallis dolor ut hendrerit rutrum. Praesent eu ipsum aliquet, fringilla dolor sit amet, suscipit enim. Aenean pharetra sagittis magna ut molestie. Ut aliquam massa eget magna feugiat congue.\r\n\r\n![](http://lsco.scene7.com/is/image/lsco/2017_H1_LP_Skate_Video_Detroit_Placeholder_DESKTOP?$full%2Djpeg$&amp;amp;fmt=pjpeg)\r\n\r\nNam convallis dolor ut hendrerit rutrum. Praesent eu ipsum aliquet, fringilla dolor sit amet, suscipit enim. Aenean pharetra sagittis magna ut molestie. Ut aliquam massa eget magna feugiat congue.\r\n\r\n![](https://image.redbull.com/rbcom/010/2015-05-04/1331720977298_3/0012/0/382/0/1713/1999/1500/1/detroit-skateboarding.jpg)\r\n'),
('Sea beauty', 'skarunik@fel.cvut.cz', '2017-09-15 18:16:39', 'Sea', 'http://localhost/awesome/img/post-preview/sea.jpg', 'Maecenas rhoncus blandit nulla sed bibendum. Nunc at risus luctus, cursus dui id, porttitor mauris. Nunc luctus lobortis leo eu aliquam. Duis et ultrices sapien. Nullam a orci ac odio fermentum finibus. Sed id erat lorem.', 'Sea beauty\r\n============\r\n\r\nMaecenas rhoncus blandit nulla sed bibendum. Nunc at risus luctus, cursus dui id, porttitor mauris. Nunc luctus lobortis leo eu aliquam. Duis et ultrices sapien. Nullam a orci ac odio fermentum finibus. Sed id erat lorem.\r\n\r\n![](http://tidalvisionusa.com/wp-content/uploads/2015/05/ocean-floor-background.png)\r\n\r\nMaecenas rhoncus blandit nulla sed bibendum. Nunc at risus luctus, cursus dui id, porttitor mauris. Nunc luctus lobortis leo eu aliquam. Duis et ultrices sapien. Nullam a orci ac odio fermentum finibus. Sed id erat lorem.\r\n\r\n![](https://i0.wp.com/uniteasia.org/wp-content/uploads/2017/02/post-rock-indonesia.jpg?fit=1280%2C720&amp;amp;ssl=1)\r\n\r\nMaecenas rhoncus blandit nulla sed bibendum. Nunc at risus luctus, cursus dui id, porttitor mauris. Nunc luctus lobortis leo eu aliquam. Duis et ultrices sapien. Nullam a orci ac odio fermentum finibus. Sed id erat lorem.\r\nMaecenas rhoncus blandit nulla sed bibendum. Nunc at risus luctus, cursus dui id, porttitor mauris. Nunc luctus lobortis leo eu aliquam. Duis et ultrices sapien. Nullam a orci ac odio fermentum finibus. Sed id erat lorem.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `COMMENT`
--
ALTER TABLE `COMMENT`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `PERSON`
--
ALTER TABLE `PERSON`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `POST`
--
ALTER TABLE `POST`
  ADD PRIMARY KEY (`heading`),
  ADD UNIQUE KEY `name` (`heading`),
  ADD KEY `POST_author` (`author`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `COMMENT`
--
ALTER TABLE `COMMENT`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `POST`
--
ALTER TABLE `POST`
  ADD CONSTRAINT `POST_author` FOREIGN KEY (`author`) REFERENCES `PERSON` (`email`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
