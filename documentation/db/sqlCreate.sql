CREATE TABLE PERSON (
  login CHAR  NOT NULL  ,
  passwprd INTEGER UNSIGNED  NOT NULL  ,
  admin BOOL  NOT NULL    ,
PRIMARY KEY(login));



CREATE TABLE CATEGORIE (
  name CHAR  NOT NULL  ,
  counter INTEGER UNSIGNED  NULL    ,
PRIMARY KEY(name));



CREATE TABLE COMMENTS (
  idComment INTEGER UNSIGNED  NOT NULL  ,
  date DATE  NOT NULL   AUTO_INCREMENT,
  PERSON_login CHAR  NOT NULL  ,
  text TEXT  NULL    ,
PRIMARY KEY(idComment),
  FOREIGN KEY(PERSON_login)
    REFERENCES PERSON(login)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION);



CREATE TABLE POST (
  idPost INTEGER UNSIGNED  NOT NULL   AUTO_INCREMENT,
  COMMENTS_idComment INTEGER UNSIGNED  NOT NULL  ,
  CATEGORIE_name CHAR  NOT NULL  ,
  PERSON_login CHAR  NOT NULL  ,
  name CHAR  NOT NULL  ,
  date DATE  NOT NULL    ,
PRIMARY KEY(idPost),
  FOREIGN KEY(PERSON_login)
    REFERENCES PERSON(login)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(CATEGORIE_name)
    REFERENCES CATEGORIE(name)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(COMMENTS_idComment)
    REFERENCES COMMENTS(idComment)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION);




