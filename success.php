<?php
	session_start();
?> 

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Art news, gallery">
	<meta name="keywords" content="art, artwork, photography, calligraphy, sculpture, printmaking, decorate">
	<meta name="author" content="Nikita Shkarupa">
	<link rel="icon" href="img/favicon.ico">
	<title>Awesome • Beautiful photos</title>

	<!-- Libs, fonts and custom css -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">

	<link href="css/main.css" rel="stylesheet" type="text/css" />

</head>

<body>

	<div class="container">
		<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<h1 class="text-center">Operation was successfully completed!</h1>
					<h3 class="text-center">You will be redirect in 5 seconds...</h3>
					<p class="text-center">If something went wrong, click <a href="home.php">here</a>.</p>
				</div>
				<div class="col-md-2"></div>
		</div>
	</div>
<?php
	header( "refresh:5;url=home.php" );
?>


	<!-- Bootstrap core JavaScript -->
	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
	<script src="js/bootstrap.js"></script>

</body>

</html>
