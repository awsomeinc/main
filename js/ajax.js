//Reloads section with comments after adding a new one
$('#ajax_comment').submit(function(){
	var values = $(this).serialize(); // values: post name, comment text
	$.ajax({
		type: 'POST',
		url: 'logic/add-comment.php',
		data: values,
		success: function(){
			$('#comments_section').load(document.URL +  ' #comments_section');
		}
	});
	return false;
});