
//		Forms validation

// Log in
function LoginFormValidation(){
  var a = document.forms["loginForm"]["email"].value;
  if (a == "") {
    alert("Try again, email is missing:(");
    return false;
  }
  var b = document.forms["loginForm"]["pass"].value;
  if (b == "") {
    alert("Try again, password is missing:(");
    return false;
  }
}

// Registration
function SignUpValidation(){
  var a = document.forms["sign-up-form"]["firstname"].value;
  if (a == "") {
    alert("Try again, first name is missing:(");
    return false;
  }
	var b = document.forms["sign-up-form"]["lastname"].value;
  if (b == "") {
    alert("Try again, last name is missing:(");
    return false;
  }
  var c = document.forms["sign-up-form"]["email"].value;
  if (c == "") {
    alert("Try again, email is missing:(");
    return false;
  }
  var d = document.forms["sign-up-form"]["password"].value;
  if (d == "") {
    alert("Try again, password is missing:(");
    return false;
  }
	var e = document.forms["sign-up-form"]["check-password"].value;
  if (e == "") {
    alert("Try again, re-entered password is missing:(");
    return false;
  }
	if (e != d) {
		alert("Try again, passwords are different:(")
		return false;
	}
	var f = document.forms["sign-up-form"]["birthday"].value;
  if (f == "") {
    alert("Try again, birthday is missing:(");
    return false;
  }
}

//	Create post
function CreatePostValidation(){
  var a = document.forms["create-post-form"]["heading"].value;
  if (a == "") {
    alert("Try again, first heading is missing:(");
    return false;
  }
	var b = document.forms["create-post-form"]["categorie"].value;
  if (b == "") {
    alert("Try again, categorie is missing:(");
    return false;
  }
  var c = document.forms["create-post-form"]["preview-photo"].value;
  if (c == "") {
    alert("Try again, preview photo is missing:(");
    return false;
  }
  var d = document.forms["create-post-form"]["preview-text"].value;
  if (d == "") {
    alert("Try again, preview text is missing:(");
    return false;
  }
	var e = document.forms["create-post-form"]["markdown"].value;
  if (e == "") {
    alert("Try again, markdown is missing:(");
    return false;
  }
}