//	Swapping registration & authorization
//	Sign-up block is disabled in css/sign-in.css

$('#show-sign-up').on('click', function (event) {
	$('#sign-in-block').fadeOut("slow", function () {
		$('#sign-in-block').hide();
		$('#sign-up-block').fadeIn("slow");
	});
});

$('#show-sign-in').on('click', function (event) {
	$('#sign-up-block').fadeOut("slow", function () {
		$('#sign-up-block').hide();
		$('#sign-in-block').fadeIn("slow");
	});
});
