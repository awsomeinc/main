//Scroll to top animation
$("#to-top").on('click', function (event) {
	event.preventDefault();
	var hash = ".navbar";
	$("html, body").animate({
		scrollTop: 0
	}, 400);
});


//Dropdown menu animation
var active = false;
$("#dropdown01").click(function () {
	if (active) {
		$(".dropdown-menu").fadeOut("slow", function () {});
		active = false;
	} else {
		$(".dropdown-menu").fadeIn("slow", function () {});
		active = true;
	}
});


//Scroll to sign-up animation
$("#to-sign-up").on('click', function (event) {
	event.preventDefault();
	$('html,body').animate({
			scrollTop: $("#sign-up-block").offset().top - 120
		},
		'slow');
	$(".dropdown-menu").fadeOut("slow", function () {});
	active = false;
});
$(".dropdown-item").click(function () {
	$(".navbar-toggler").addClass("collapsed");
	$(".navbar-collapse").removeClass("show");
	$(".navbar-toggler").attr("aria-expanded", "false");
});


//Change header style
function changeView(){
		if(typeof(Storage) !== "undefined") {
			if (localStorage.color) {
				if (localStorage.color == "rgb(41, 43, 44)") {
					localStorage.color = "rgb(90, 160, 190)";
					var value = localStorage.color;
					if (value !== null)$(".bg-inverse").css("background-color", value);
				} else {
					localStorage.color = "rgb(41, 43, 44)";
					var value = localStorage.color;
					if (value !== null)$(".bg-inverse").css("background-color", value);
					}
			}
		}
}