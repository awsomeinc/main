<?php
	session_start();
	if (isset($_SESSION['user-email'])){
		header("Location: home.php");
	}
?> 

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Art news, gallery">
	<meta name="keywords" content="art, artwork, photography, calligraphy, sculpture, printmaking, decorate">
	<meta name="author" content="Nikita Shkarupa">
	<link rel="icon" href="img/favicon.ico">
	<title>Awesome • Beautiful photos</title>

	<!-- Libs, fonts and custom css -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">

	<!-- Datepicker -->
	<link href="libs/datepicker/dist/css/datepicker.css" rel="stylesheet" type="text/css">

	<link href="css/main.css" rel="stylesheet" type="text/css" />
	<link href="css/media.css" rel="stylesheet" type="text/css" />

</head>

<body>
	
	<!-- Navigation -->
	<?php include 'components/header.php';?>

	<!-- Carousel -->
	<?php include 'components/index-carousel.html';?>

	<!-- Features -->
	<div class="container marketing">
		<!-- Three columns of text below the carousel -->
		<div class="row">
			<div class="col-lg-4">
				<img class="rounded-circle" src="img/person1.jpg" alt="Generic placeholder image" width="140" height="140">
				<h2>Lukas Kai</h2>
				<p>Officia reiciendis velit, nobis veniam a quia, asperiores, quasi quae rerum impedit ducimus iste cum. Minus quasi iste nihil facilis veniam qui!</p>
			</div>
			<!-- /.col-lg-4 -->
			<div class="col-lg-4">
				<img class="rounded-circle" src="img/person2.jpg" alt="Generic placeholder image" width="140" height="140">
				<h2>Michi Walter</h2>
				<p>Nesciunt minima fuga amet perspiciatis tempore explicabo aliquid unde veritatis neque soluta, eum consequuntur quibusdam quod expedita rerum, magni debitis labore animi.</p>
			</div>
			<!-- /.col-lg-4 -->
			<div class="col-lg-4">
				<img class="rounded-circle" src="img/person3.jpg" alt="Generic placeholder image" width="140" height="140">
				<h2>Teodora Franka</h2>
				<p>Doloribus magni qui voluptate tenetur mollitia dolor voluptatum enim aperiam molestiae libero repellat dolores nemo ipsum facilis, ipsa. Ab aliquid iusto illo, culpa iste eligendi ducimus totam!</p>
			</div>
			<!-- /.col-lg-4 -->
		</div>
		<!-- /.row -->

		<hr class="featurette-divider">

		<!-- Sign-up block -->
		<?php include 'components/sign-up.php';?>

		<!-- Content -->
		<hr class="featurette-divider">

		<div class="row featurette">
			<div class="col-md-7">
				<h2 class="featurette-heading">Create your gallery. <span class="text-muted">It'll blow someone's mind.</span></h2>
				<p class="lead">Nulla facilisi. Pellentesque vel consequat tortor. Vivamus porttitor, mauris nec condimentum gravida, nulla ex semper velit, non porta enim leo quis urna. Pellentesque eget quam a diam lacinia posuere. Aenean mollis molestie lacus.</p>
			</div>
			<div class="col-md-5">
				<img class="featurette-image img-fluid mx-auto" src="img/photos-in-frames.jpg" alt="Generic placeholder image">
			</div>
		</div>

		<hr class="featurette-divider">

		<div class="row featurette">
			<div class="col-md-7 push-md-5">
				<h2 class="featurette-heading">Make a portfolio. <span class="text-muted">See your progress.</span></h2>
				<p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
			</div>
			<div class="col-md-5 pull-md-7">
				<img class="featurette-image img-fluid mx-auto" src="img/photographer-1.jpg" alt="Generic placeholder image">
			</div>
		</div>

		<hr class="featurette-divider">

		<div class="row featurette">
			<div class="col-md-7">
				<h2 class="featurette-heading">Share you moments. <span class="text-muted">They are awesome!</span></h2>
				<p class="lead">Morbi vel imperdiet orci. Nam maximus metus leo, hendrerit semper lectus venenatis in. Integer tincidunt elementum bibendum. Duis sit amet justo ac metus congue dapibus. Nulla quis neque sem. Morbi fermentum ipsum ut justo consectetur, eu facilisis.</p>
			</div>
			<div class="col-md-5">
				<img class="featurette-image img-fluid mx-auto" src="img/photographer-2.jpg" s alt="Generic placeholder image">
			</div>
		</div>

		<hr class="featurette-divider">

		<!-- FOOTER -->
		<footer>
			<p class="float-right"><a id="to-top" href="#">Back to top</a></p>
			<p>&copy; 2017 Awesome, Inc.</p>
		</footer>

	</div>

	<!-- Bootstrap core JavaScript -->
	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
	<!-- Scripts -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="https://use.fontawesome.com/1621f9eb2b.js"></script>
	<!-- Datepicker -->
	<script src="libs/datepicker/dist/js/datepicker.js"></script>
	<script src="js/validation.js"></script>
	<script src="js/common.js"></script>

</body>

</html>
