<!-- Trying to load data fron local storage -->
<script>
	window.onload = function() {
		var inputs = ["#firstname", "#lastname", "#email", "#birthday"];
		$.each( inputs, function( i, l ){
			var value = localStorage.getItem(l);
			if (value !== null) $(l).val(value);
		});
	}
</script>

<div class="container sign-up" id="sign-up-block">
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-4 well well-sm">
			<legend><a href="#"><i class="glyphicon glyphicon-globe"></i></a> Registration </legend>
			<form class="form" name="sign-up-form" action="/awesome/logic/sign-up.php" method="POST" onsubmit="return SignUpValidation()">
				<div class="row">
					<div class="col-xs-6 col-md-6">
						<label for="firstname"></label>
						<input class="form-control" id="firstname" name="firstname" placeholder="First Name" type="text" pattern="^[a-zA-Z]{4,15}$" title="Name field should contain at least 4 letters." required />
					</div>
					<div class="col-xs-6 col-md-6">
						<label for="lastname"></label>
						<input class="form-control" id="lastname" name="lastname" placeholder="Last Name" type="text" pattern="^[a-zA-Z]{4,15}$" title="Last Name field should contain at least 4 letters." required />
					</div>
				</div>
				<label for="email"></label>
				<input class="form-control" id="email" name="email" placeholder="Your Email" type="email" required /> <!-- Every regular expression for email validation is missing something. -->
				<label for="password"></label>
				<input class="form-control" id="password" name="password" placeholder="New Password" type="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" title="Password should contain at least 1 UpperCase letter, 1 LowCase and a number." required />
				<label for="check-password"></label>
				<input class="form-control" id="check-password" name="check-password" placeholder="Re-enter Password" type="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" title="Password should contain at least 1 UpperCase letter, 1 LowCase and a number." required />
				<div class="row">
					<div class="col-md-12">
					
						<!-- Datepicker start -->
						<div class="input-group date">
							<label for="birthday"></label>
							<input type="text" class="datepicker-here form-control" id="birthday" name="birthday" placeholder="Your birth date" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" title="Enter your birthday with following format: DD.MM.YYYY" required />
							<div class="input-group-addon">
								<i class="fa fa-calendar" aria-hidden="true"></i>
							</div>
						</div>
						<!-- Datepicker esnd -->
						
					</div>
				</div>
				<br />
				<button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
			</form>
		</div>
		<div class="col-md-2"></div>
		<div class="col-xs-12 col-sm-12 col-md-5 well well-sm">
			<h2 class="featurette-heading">Sign up today. <span class="text-muted">Become a part of the Awesome team!</span></h2>
		</div>
	</div>
</div>

<!-- Saving inputs' value to the local storage befor leaving the page -->
<script>
	window.onbeforeunload = function(){
		var inputs = ["#firstname", "#lastname", "#email", "#birthday"];
		$.each( inputs, function( i, l ){
			var value = $(l).val();
			localStorage.setItem(l, value);
		});
	}
</script>