<nav id="top" class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">

<!-- Setting selected color for header -->
<script>
	if(localStorage.color){
		document.getElementById("top").style.backgroundColor = localStorage.color;
	}
</script>

	<!-- Small devices menu -->
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<!-- End -->
	
	<a class="navbar-brand" href="index.php">Awesome</a>
	<div class="collapse navbar-collapse" id="navbarCollapse">
	
		<!-- Menu -->
		<ul class="navbar-nav mr-auto">
			<?php
			if (isset($_SESSION['user-email'])) { // Checking if user authorized or not
			?>
				<li class="nav-item">
					<a class="nav-link" href="home.php">Home <span class="sr-only"></span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="categories.php">Categories <span class="sr-only"></span></a>
				</li>
				
				<!-- Logout -->
				<li class="nav-item dropdown">
					<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<?php
							echo $_SESSION['user-email'];
						?>
					</a>
					<div class="dropdown-menu" aria-labelledby="dropdown01">
						<a class="dropdown-item" href="logic/logout.php">Exit</a>
					</div>
				</li>
				
				<?php
					} else {
				?>
					<!-- Authorization -->
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Authorization</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
							<a class="dropdown-item" id="to-sign-up" href="#">Sign Up</a>
							<a class="dropdown-item" href="sign-in.php">Sign In</a>
						</div>
					</li>
				<?php
					};
				?>
		</ul>
		
<!-- Button to hange color for header -->
  	<button type="button" class="btn btn-primary float-left" onclick="changeView()">Change view</button>
	</div>
</nav>
