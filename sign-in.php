<?php 
	session_start();
	if (isset($_SESSION['user-email'])){
		header("Location: home.php");
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Art news, gallery">
	<meta name="keywords" content="art, artwork, photography, calligraphy, sculpture, printmaking, decorate">
	<meta name="author" content="Nikita Shkarupa">
	<link rel="icon" href="img/favicon.ico">
	<title>Awesome • Beautiful photos</title>

	<!-- Libs, fonts and custom css -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">

	<!-- Datepicker -->
	<link href="libs/datepicker/dist/css/datepicker.css" rel="stylesheet" type="text/css">

	<link href="css/main.css" rel="stylesheet" type="text/css" />
	<link href="css/media.css" rel="stylesheet" type="text/css" />
	<link href="css/sign-in.css" rel="stylesheet" type="text/css" />

</head>

<body>

	<?php include 'components/header.php';?>

	<!-- Trying to load data fron local storage -->
	<script>
		window.onload = function() {
			var inputs = ["#email_auth", "#email_reg", "#name_reg", "#surname_reg", "#birthday_reg"];
			$.each(inputs, function(i, l) {
				var value = localStorage.getItem(l);
				if (value !== null) $(l).val(value);
			});
		}
	</script>

	<!-- Authorization block -->
	<div class="container marketing sign-in-page">
		<div class="row featurette" id="sign-in-block">
			<div class="col-md-7 push-md-5">
				<h2 class="display-4">Hello there!</h2>
				<p class="lead">Donec rhoncus et odio nec sollicitudin. Sed cursus enim eget nibh dignissim, eget suscipit nunc dapibus. Suspendisse eu felis enim. Phasellus pellentesque pretium magna ut rhoncus. Curabitur lobortis elit sit amet sem convallis lobortis. Maecenas ornare sem ut dolor facilisis, ut imperdiet sapien sagittis. Nulla magna lorem, molestie quis libero vel, luctus rutrum neque. Praesent sodales quis est ultricies gravida. Mauris ornare risus quis lacus mattis.</p>
			</div>
			<div class="col-md-5 pull-md-7">
				<legend><a href="#"><i class="glyphicon glyphicon-globe"></i></a> Authorization form </legend>
				<form action="logic/authorization.php" method="post" class="form" role="form" onsubmit="return LoginFormValidation()">
					<!-- No pattern for email because every regular expression for email validation is missing something. -->
					<label for="email_auth"></label>
					<input class="form-control" id="email_auth" name="email" placeholder="Your Email" type="email" required />
					<!-- No pattern for password because of some existing password-->
					<label for="password_auth"></label>
					<input class="form-control" name="password" id="password_auth" placeholder="Your Password" type="password" title="Password should contain at least 1 UpperCase letter, 1 LowCase and a number." required />

					<!-- In case of wrong input -->
					<?php
						if (isset($_SESSION['auth-error'])){
							echo "<span class=\"text-danger\">".$_SESSION['auth-error']."</span><br/>";
							unset($_SESSION['auth-error']);
						}
					?>

						<br />
						<button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
						<br />
				</form>
				<div class="row">
					<div class="col-md-6">
						<button class="btn btn-lg btn-secondary btn-block disabled" type="submit">Restore password</button>
					</div>
					<div class="col-md-6">
						<button class="btn btn-lg btn-secondary btn-block" type="submit" id="show-sign-up">Registraration</button>
					</div>
				</div>
			</div>
		</div>

		<!-- Registration block -->
		<div class="container sign-up" id="sign-up-block">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 well well-sm">
					<legend><a href="#"><i class="glyphicon glyphicon-globe"></i></a> Registration </legend>
					<form class="form" name="sign-up-form" action="logic/sign-up.php" method="post" onsubmit="return SignUpValidation()">
						<div class="row">
							<div class="col-xs-6 col-md-6">
								<label for="name_reg"></label>
								<input class="form-control" name="firstname" id="name_reg" placeholder="First Name" type="text" pattern="^[a-zA-Z]{4,15}$" title="Name field should contain at least 4 letters." required />
							</div>
							<div class="col-xs-6 col-md-6">
								<label for="surname_reg"></label>
								<input class="form-control" name="lastname" id="surname_reg" placeholder="Last Name" type="text" pattern="^[a-zA-Z]{4,15}$" title="Last Name field should contain at least 4 letters." required />
							</div>
						</div>

						<!-- No pattern for email, becouse every regular expression for email validation is missing something. -->
						<label for="email_reg"></label>
						<input class="form-control" name="email" id="email_reg" placeholder="Your Email" type="email" required />
						<label for="password_reg"></label>
						<input class="form-control" name="password" id="email_reg" placeholder="New Password" type="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" title="Password should contain at least 1 UpperCase letter, 1 LowCase and a number." required />
						<label for="check_password_reg"></label>
						<input class="form-control" name="check-password" id="check_password_reg" placeholder="Re-enter Password" type="password" pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s).*$" title="Password should contain at least 1 UpperCase letter, 1 LowCase and a number." required />
						<div class="row">
							<div class="col-md-12">

								<!-- Datepicker start -->
								<div class="input-group date">
									<label for="birthday_reg"></label>
									<input type="text" class="datepicker-here form-control" name="birthday" id="birthday_reg" placeholder="Your birth date" pattern="(0[1-9]|1[0-9]|2[0-9]|3[01]).(0[1-9]|1[012]).[0-9]{4}" title="Enter your birthday with following format: DD.MM.YYYY" required />
									<div class="input-group-addon">
										<i class="fa fa-calendar" aria-hidden="true"></i>
									</div>
								</div>
								<!-- Datepicker end -->

							</div>
						</div>
						<br />
						<button class="btn btn-lg btn-primary btn-block" type="submit">Sign up</button>
					</form>
					<br />
					<button class="btn btn-lg btn-secondary btn-block" type="submit" id="show-sign-in">Sign in</button>
				</div>
				<div class="col-md-2"></div>
				<div class="col-xs-12 col-sm-12 col-md-5 well well-sm">
					<h2 class="featurette-heading">Sign up today. <span class="text-muted">Become a part of the Awesome team!</span></h2>
				</div>
			</div>
		</div>
		<!--Registration block ends-->

		<hr class="featurette-divider">
		<div class="row featurette">
			<div class="col-md-12">
				<p>&copy; 2017 Awesome, Inc.</p>
			</div>
		</div>
	</div>

	<!-- Saving inputs' value to the local storage befor leaving the page -->
	<script>
		window.onbeforeunload = function() {
			var inputs = ["#email_auth", "#email_reg", "#name_reg", "#surname_reg", "#birthday_reg"];
			$.each(inputs, function(i, l) {
				var value = $(l).val();
				localStorage.setItem(l, value);
			});
		}
	</script>

	<!-- Bootstrap core JavaScript -->
	<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
	<!-- Scripts -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="https://use.fontawesome.com/1621f9eb2b.js"></script>
	<!-- Datepicker -->
	<script src="libs/datepicker/dist/js/datepicker.js"></script>
	<script src="js/sign-in.js"></script>
	<script src="js/validation.js"></script>

</body>

</html>
