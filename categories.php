<?php
	session_start();
	if (!isset($_SESSION['user-email'])){
		header("Location: index.php");
	}
?> 
 

<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Art news, gallery">
	<meta name="keywords" content="art, artwork, photography, calligraphy, sculpture, printmaking, decorate">
	<meta name="author" content="Nikita Shkarupa">
	<link rel="icon" href="img/favicon.ico">
	<title>Awesome • Beautiful photos</title>

	<!-- Libs, fonts and custom css -->
	<link href="css/bootstrap.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">

	<!-- Datepicker -->
	<link href="libs/datepicker/dist/css/datepicker.css" rel="stylesheet" type="text/css">

	<link href="css/main.css" rel="stylesheet" type="text/css" />
	<link href="css/gallery.css" rel="stylesheet" type="text/css" />
	<link href="css/media.css" rel="stylesheet" type="text/css" />

</head>
		

	<body>

		<!-- Navigation -->
		<?php include 'components/header.php';?>


		<!-- Page Content -->
		<div class="container wrapper">

			<div class="container">
				<div class="heading">
					<h2>Categorie catalog</h2>
					<h4>Select categorie to find what you are looking for.</h4>
				</div>

				<div class="row">
					<div class="col-md-4">
						<div class="thumbnail">	
							<a href="home.php?categorie=Animals">
								<img src="img/catalog-animals.jpg" alt="Lights" style="width:100%">
								<div class="caption">
									<h3>Animals</h3>
									<p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="thumbnail">
							<a href="home.php?categorie=City">
								<img src="img/catalog-city.jpg" alt="Nature" style="width:100%">
								<div class="caption">
									<h3>City</h3>
									<p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="thumbnail">
							<a href="home.php?categorie=Nature">
								<img src="img/catalog-forest.jpg" alt="Fjords" style="width:100%">
								<div class="caption">
									<h3>Nature</h3>
									<p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="thumbnail">
							<a href="home.php?categorie=Sea">
								<img src="img/catalog-sea.jpg" alt="Lights" style="width:100%">
								<div class="caption">
									<h3>Sea</h3>
									<p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="thumbnail">
							<a href="home.php?categorie=People">
								<img src="img/catalog-people.jpg" alt="Nature" style="width:100%">
								<div class="caption">
									<h3>People</h3>
									<p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
								</div>
							</a>
						</div>
					</div>
					<div class="col-md-4">
						<div class="thumbnail">
							<a href="home.php?categorie=Architecture">
								<img src="img/catalog-architecture.jpg" alt="Fjords" style="width:100%">
								<div class="caption">
									<h3>Architecture</h3>
									<p>Lorem ipsum donec id elit non mi porta gravida at eget metus.</p>
								</div>
							</a>
						</div>
					</div>
				</div>
			</div>
			
			<!-- FOOTER -->
			<hr class="featurette-divider">
			<footer>
				<p class="float-right"><a id="to-top" href="#">Back to top</a></p>
				<p>&copy; 2017 Awesome, Inc.</p>
			</footer>
			
		</div>
		<!-- /.container -->

		<!-- Bootstrap core JavaScript -->
		<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
		<!-- Scripts -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="js/bootstrap.js"></script>
		<script src="https://use.fontawesome.com/1621f9eb2b.js"></script>
		<script src="js/common.js"></script>

	</body>

	</html>
