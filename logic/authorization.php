<?php 
	session_start();

//		Create connection
	include 'db_params.php';
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
	}

//		Taking parameters
	$email = str_replace(' ', '', htmlspecialchars($_POST['email']));
	$password = htmlspecialchars($_POST['password']);

//		Comparing hashes
	if(isset($email, $password)){
		$query = "SELECT password FROM PERSON WHERE email='$email'";
		$result = mysqli_query($conn, $query);
		if(mysqli_num_rows($result) == 1){
			$row = mysqli_fetch_array($result);
			if (password_verify($password, $row['password'])) {
	//			Then authorized
				$_SESSION['user-email'] = $email;
				header("Location: ../home.php");
		} else {
			$_SESSION['auth-error'] = 'Wrong password or email, try again!';
			header("Location: ../sign-in.php");;
			}
		}
	}

	//	Closing connection
	$conn->close();
?>