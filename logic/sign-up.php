<?php
  session_start();

	addUser();

  function addUser() {
		
		$firstname = str_replace(' ', '', htmlspecialchars($_POST['firstname'])); // Delete spaces with str_replace
		$lastname = str_replace(' ', '', htmlspecialchars($_POST['lastname']));
		$email = str_replace(' ', '', htmlspecialchars($_POST['email']));
		$passwrd = htmlspecialchars($_POST['password']);
		$checkpass = htmlspecialchars($_POST['check-password']);
//		Checking passwords
		if($passwrd != $checkpass){
			$error = true;
			echo 'Passwords do not match.';
		}
		
//		Hashing password
		$passhash = password_hash($passwrd, PASSWORD_DEFAULT);
			
		$date = htmlspecialchars($_POST['birthday']);
		$birthday = date("Y-m-d", strtotime($date));
		$admin = false;
		
		// Create connection
		include 'db_params.php';
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
		}
		
//		Checking parameters
    if(isset($firstname, $lastname, $email, $passhash, $birthday, $admin)){
//		Checking email uniqueness
			$query = "SELECT email FROM PERSON WHERE email='$email'";
			$result = mysqli_query($conn, $query);
			$count = mysqli_num_rows($result);
			if($count!=0){
				$error = true;
				?>
					<p>Provided Email is already in use.</p>
					<p>Click <a href="../sign-in.php">here</a> to complete the form again.</p>
				<?php
			}
    } else {
			echo 'Sign-up error!';
		}
		
//		Writing into database
		if(!$error) {
		  $query = "INSERT INTO PERSON (firstname, lastname, email, password, birthday, admin) VALUES('$firstname','$lastname','$email','$passhash','$birthday', '$admin')";
		  $res = mysqli_query($conn, $query);
		  if ($res) {
				$_SESSION['user-email'] = $email;
				$_SESSION['authorized'] = true;
				header("Location: ../success.php");
		  } else {
				$error = true;
				echo("Something went wrong, try again later...");
			} 
		}
		
		//	Closing connection
		$conn->close();
  }
?>