<?php
  session_start();

		// Set params
		$heading = htmlspecialchars($_POST['heading']);
		$author = $_SESSION['user-email'];
		$date = date("Y-m-d H:i:s");
		$categorie = htmlspecialchars($_POST['categorie']);
		$previewPhoto = str_replace(' ', '', htmlspecialchars($_POST['preview-photo']));
		$previewText = htmlspecialchars($_POST['preview-text']);
		$markdown = htmlspecialchars($_POST['markdown']);

		// Create connection
		include 'db_params.php';
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
		}

		// Checking parameters
  	if($heading != '' && $author != '' && $date != '' && $categorie != '' && $previewPhoto != '' && $previewText != '' && $markdown != ''){

			// Checking heading uniqueness
			$query = "SELECT heading FROM POST WHERE heading='$heading'";
			$result = mysqli_query($conn, $query);
			$count = mysqli_num_rows($result);
			if($count!=0){
				$error = true;
				?>
					<p>This heading is already in use.</p>
					<p>Click <a href="../create_post.php">here</a> to return.</p>
				<?php
			}
    } else {
			echo 'Validation error!'; // Empty parameter passed validation!
		}
		
//		Writing into database
		if(!isset($error)) {
		  $query = "INSERT INTO POST (heading, author, date, categorie, previewPhoto, previewText, markdown) VALUES('$heading', '$author', '$date', '$categorie', '$previewPhoto', '$previewText', '$markdown')";
		  $res = mysqli_query($conn, $query);
		  if ($res) {
				header("Location: ../success.php");
		  } else {
				$error = true;
				echo("Something went wrong, try again later...");
			} 
		}

		//	Closing connection
		$conn->close();
?>