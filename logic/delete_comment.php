<?php
  session_start();
		
	$heading = htmlspecialchars($_GET['post']);
	$id = htmlspecialchars($_GET['comment']);
	$author = $_SESSION['user-email'];

	// Create connection
	include 'db_params.php';
	$conn = new mysqli($servername, $username, $password, $dbname);
	// Check connection
	if ($conn->connect_error) {
			die("Connection failed: " . $conn->connect_error);
	}

//		Checking parameters
	if($heading != '' && $author != '' && $id != ''){
		$query = "SELECT author FROM COMMENT WHERE id = '$id'";
		$result = mysqli_query($conn, $query);
		if($row = mysqli_fetch_array($result)){
			if($row['author'] == $author){
				$error = false;
			} else {
				$error = true;
			}
		} else {
			$error = true;
		}
		if(!$error){
			$query = "DELETE FROM COMMENT WHERE id = '$id'";
			$reult = mysqli_query($conn, $query);
			if($result){
				header('Location: ' . $_SERVER['HTTP_REFERER']);
			} else {
				echo "Error. Comment wasn't deleted";
			}
		}
	} else {
		echo 'Something went wrong. Check input parameters.';
	}

	//	Closing connection
	$conn->close();
?>