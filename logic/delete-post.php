<?php 
  session_start();
		
		$post_heading = htmlspecialchars($_POST['hidden-post-heading']);
		
		// Create connection
		include 'db_params.php';
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
				die("Connection failed: " . $conn->connect_error);
		}
		
    if($post_heading){
			
			$query = "DELETE FROM POST WHERE heading='$post_heading'";
			if ($conn->query($query) === TRUE) {
					header( "Location: ../success.php");
			} else {
					echo "Error deleting record: ";
			}
		}

		//	Closing connection
		$conn->close();
?>