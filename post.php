<?php
	session_start();
	if (!isset($_SESSION['user-email'])){
		header("Location: index.php");
	}
?>

	<!DOCTYPE html>
	<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="Art news, gallery">
		<meta name="keywords" content="art, artwork, photography, calligraphy, sculpture, printmaking, decorate">
		<meta name="author" content="Nikita Shkarupa">
		<link rel="icon" href="img/favicon.ico">
		<title>Awesome • Beautiful photos</title>

		<!-- Libs, fonts and custom css -->
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet" type="text/css" />
		<link href="css/post.css" rel="stylesheet" type="text/css" />
		<link href="css/media.css" rel="stylesheet" type="text/css" />

	</head>

	<body>

		<!-- Navigation -->
		<?php include 'components/header.php';?>

		<!-- Features -->
		<div class="container marketing">

			<!-- Page Content -->
			<div class="container">
				<div class="row">

					<!-- Blog Entries Column -->
					<div class="col-md-12">

						<!--Posts from  db-->
						<?php
						// Create connection
							include 'logic/db_params.php';
							$conn = new mysqli($servername, $username, $password, $dbname);
							// Check connection
							if ($conn->connect_error) {
							die("Connection failed: " . $conn->connect_error);
							}
							$heading = htmlspecialchars($_GET['post']);
							$query = "SELECT * FROM `POST` WHERE heading = '$heading'";
							$result = mysqli_query($conn, $query);
							if($row = mysqli_fetch_array($result)){
								echo "<div class=\"post thumbnail\">"; 
									echo "<div class=\"post-section\">";
							$markdown = $row['markdown'];
										echo "<textarea class=\"post-text dn\" id=\"post-text\" rows=\"20\" cols=\"20\">" . $markdown . "</textarea>";
										echo "<div id=\"md2html\" class=\"post-text\"></div>";
									echo "</div>";
									echo "<div class=\"post-footer\">";
										echo "<span class=\"text-muted\">" . "Posted on " . $row['date'] . " by " . $row['author'] . "</span>";
										if($_SESSION['user-email'] ==  $row['author']){
											echo "<form class=\"form float-right\" action=\"/awesome/logic/delete-post.php\" method=\"POST\">";
												echo "<input class=\"dn\" name=\"hidden-post-heading\" type=\"text\" value=\"".$row['heading']."\">";
												echo "<button type=\"submit\" class=\"btn btn-danger\">Delete</button>";
											echo "</form>";
										}
									echo "</div>";
								echo "</div>";
							}
							?>
					</div>

					<!-- Comments section -->
					<div class="col-md-12">
					
						<!-- Taking comments from db -->
						<div class="thumbnail">
							<div id="comments_section">
							<?php
							$query_comment = "SELECT COUNT(id) FROM `COMMENT` WHERE post = '$heading' ORDER BY date DESC";
							$result_comment = mysqli_query($conn, $query_comment);	
							if($result_count =! 0){
								$query = "SELECT * FROM `COMMENT` WHERE post = '$heading'";
								$result = mysqli_query($conn, $query);
								while($row = mysqli_fetch_array($result)){
									echo "<div class=\"comment\">";
										echo "<p>" . $row['text'] . "</p>";
										echo "<p class=\"comment-author\">" . "Added on " . $row['date'] . " by " . $row['author'] . "</p>";
									if ($_SESSION['user-email'] == $row['author']){
										echo "<form class=\"form\" action=\"/awesome/logic/delete_comment.php?post=".$heading."&comment=".$row['id']."\" method=\"POST\">";
											echo "<input class=\"dn\" name=\"hidden-post-heading\" type=\"text\" value=\"".$row['text']."\">";
											echo "<button type=\"submit\" class=\"btn btn-danger\">Delete</button>";
										echo "</form>";
									}
									echo "</div>";
								}
							} else {
								echo "<p class=\"comment\">No comments yet.</p>";
							}
							//	Closing connection
								$conn->close();
							?>
							</div>
						</div>
						<!-- ./comments from db -->
							
						<!-- Add new comment -->
						<div class="thumbnail">
							<div class="new_comment">
								<form id="ajax_comment" class="form" method="POST" name="new_comment">
								<?php
									echo "<input type=\"text\" name=\"post\" class=\"dn\" value=\"".$heading."\">";
								?>
									<textarea name="comment_text" id="comment_text" class="form-control" rows="7" required></textarea>
									<button class="btn btn-primary float-right" type="submit">Add comment</button>
								</form>
							</div>
						</div>
						<!-- ./adding new comment-->
						
					</div>
					<!-- ./comments -->

				</div>
				<!-- /.container -->
				<hr class="featurette-divider">

				<!-- Footer -->
				<footer>
					<p class="float-right"><a id="to-top" href="#">Back to top</a></p>
					<p>&copy; 2017 Awesome, Inc.</p>
				</footer>
			</div>

			<!-- Markdown 2 html -->
			<script src="node_modules/markdown/lib/markdown.js"></script>
			<script>
				function Editor(input, preview) {
					this.update = function() {
						preview.innerHTML = markdown.toHTML(input.value);
					};
					input.editor = this;
					this.update();
				}
				var $ = function(id) {
					return document.getElementById(id);
				};
				new Editor($("post-text"), $("md2html"));
			</script>

			<!-- Bootstrap core JavaScript -->
			<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
			<script src="https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js"></script>
			<!-- Scripts -->
			<script src="js/jquery-3.1.1.min.js"></script>
			<script src="js/bootstrap.js"></script>
			<script src="https://use.fontawesome.com/1621f9eb2b.js"></script>
			<script src="js/ajax.js"></script>
			<script src="js/common.js"></script>

		</div>

	</body>

	</html>
