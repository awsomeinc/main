<?php
	session_start();
	if (!isset($_SESSION['user-email'])){
		header("Location: index.php");
	}
?>
	<!DOCTYPE html>
	<html lang="en">

	<head>

		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="Art news, gallery">
		<meta name="keywords" content="art, artwork, photography, calligraphy, sculpture, printmaking, decorate">
		<meta name="author" content="Nikita Shkarupa">
		<link rel="icon" href="img/favicon.ico">
		<title>Awesome • Beautiful photos</title>

		<!-- Libs, fonts and custom css -->
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="https://fonts.googleapis.com/css?family=Pacifico" rel="stylesheet">
		<link href="css/main.css" rel="stylesheet" type="text/css" />
		<link href="css/home.css" rel="stylesheet" type="text/css" />
		<link href="css/media.css" rel="stylesheet" type="text/css" />

	</head>

	<body>

		<!-- Navigation -->
		<?php include 'components/header.php';?>

		<!-- Features -->
		<div class="container marketing">

			<!-- Page Content -->
			<div class="container">
				<div class="row">
					<main class="col-md-8">

						<!--Posts from  db-->
						<?php
						// Create connection
							include 'logic/db_params.php';
							$conn = new mysqli($servername, $username, $password, $dbname);
							
							// Check connection
							if ($conn->connect_error) {
								die("Connection failed: " . $conn->connect_error);
							}

							if(isset($_GET['categorie'])){
								$categorie = $_GET['categorie'];
								$query = "SELECT * FROM POST WHERE categorie = '$categorie'";
							} else {
								$query = "SELECT * FROM POST";
							}
							// Pagination logic
							$result = mysqli_query($conn, $query);
							$num_rows = mysqli_num_rows($result); // Number of posts for pagination
							$per_page = 3; // Posts per page
							$num_pages = ceil($num_rows/$per_page); // Number of pages
							if(isset($_GET['page'])){
								$page = htmlspecialchars(intval($_GET['page'])); // Taking int value to prevent xss through get request
							} else  {
								$page = 1;
							}
							$offset = ($page - 1) * $per_page; // Posts offset
  						$url_page = "/awesome/home.php?page="; // URL in case of pagination

							// Making posts
							if(isset($categorie)){
								$query2 = "SELECT * FROM POST WHERE categorie = '$categorie' ORDER BY date DESC LIMIT $per_page OFFSET $offset";
								echo "<h1 class=\"widget-heading\">" . $categorie . "</h1>";
							} else {
								$query2 = "SELECT * FROM POST ORDER BY date DESC LIMIT $per_page OFFSET $offset"; // Posts that we need on page
							}
							$result2 = mysqli_query($conn, $query2);
							while($row = mysqli_fetch_array($result2)){	
								echo "<div class=\"post\">";
									echo "<h2 class=\"post-section\">" . $row['heading'] . "</h2>";
									echo "<img class=\"post-section img-fluid\" src=\"" .$row['previewPhoto'] . "\" alt=\"Card image cap\">";
									echo "<div class=\"post-section\">";
										echo "<p class=\"post-text\">" . $row['previewText'] . "</p>";
									echo "</div>";
									echo "<div class=\"post-footer\">";
										echo "<span class=\"text-muted\">" . "Posted on " . $row['date'] . " by " . $row['author'] . "</span>";
										echo "<form class=\"form float-right\" action=\"/awesome/logic/post-routing.php\" method=\"POST\">";
											echo "<a href=\"post.php?post=".$row['heading']."\" class=\"btn btn-primary\">Read more &rarr;</a>";
										echo "</form>";
									echo "</div>";
								echo "</div>";
							}

						//	Closing connection
							$conn->close();
						?>

							<!-- Pagination -->
							<form action="logic/pagination.php" method="post" class="form">
								<ul class="pagination justify-content-center mb-4">
									<?php
										if(isset($categorie)){
											for($i = 1; $i <= $num_pages; $i++){
											if(isset($page) && $i == $page){
												echo "<li class=\"page-item disabled\"><a href=\"\" class=\"page-link\">" . $i . "</a></li>";
											} else {
												echo "<li class=\"page-item\"><a href=\"".$url_page.$i."&categorie=".$categorie."\" class=\"page-link\">" . $i . "</a></li>";
											}
										};
										} else {
											for($i = 1; $i <= $num_pages; $i++){
												if(isset($page) && $i == $page){
													echo "<li class=\"page-item disabled\"><a href=\"\" class=\"page-link\" id=\"$i\">" . $i . "</a></li>";
												} else {
													echo "<li class=\"page-item\"><a href=\"".$url_page.$i."\" class=\"page-link\" id=\"$i\">" . $i . "</a></li>";
												}
											};
										}
									?>
								</ul>
							</form>
							<!-- End pagination -->

					</main>
					<!-- End main section -->

					<!-- Right Column -->
					<aside class="col-md-4">
						<div class="my-4">
							<h5 class="widget-heading text-center">Find post...</h5>
							<div>
								<div class="input-group">
									<input type="text" class="form-control" placeholder="Search for...">
									<span class="input-group-btn">
								  	<button class="btn btn-secondary" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
									</span>
								</div>
							</div>
						</div>
						<hr>
						<div class="my-4">
							<h5 class="widget-heading text-center"><a href="create_post.php">Add new post <i class="fa fa-plus" aria-hidden="true"></i></a></h5>
						</div>
					</aside>

				</div>
				<!-- /.row -->
			</div>
			<!-- /.container -->

			<!-- FOOTER -->
			<hr class="featurette-divider">

			<footer>
				<p class="float-right"><a id="to-top" href="#">Back to top</a></p>
				<p>&copy; 2017 Awesome, Inc.</p>
			</footer>

		</div>


		<!-- Scripts -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="js/bootstrap.js"></script>
		<script src="https://use.fontawesome.com/1621f9eb2b.js"></script>
		<script src="js/common.js"></script>

	</body>

	</html>
